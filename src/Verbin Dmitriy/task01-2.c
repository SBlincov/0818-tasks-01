// �������� ���������, ����������� �������� ����� � ������������ ������� (����'�����) � ��������� � ����������� (��). � ����� ���� 12 ������, � 1 ����� 2.54 ��. ��� ������������ ����� ��������� ������ �������� ��������� �� ������.
#include <stdio.h>
#include <locale.h>
#define MAX_STR 50
#define CM_IN_INCH 2.54;

int main() {

	int i;
	int delimeter = -1; 
	int ft = 0, inches = 0;
	double cm_height = 0;
	char str[MAX_STR];
	char str_correct = 0;

	setlocale(LC_ALL, "Rus");

	do {
		puts(">> ������� ���� � ������������ ������� (������ ����'�����):");
		fgets(str, MAX_STR, stdin);
		str[strlen(str) - 1] = 0; 

		i = 0;
		delimeter = -1; 
		while (str[i] >= '0' && str[i] <= '9' || (str[i] == '\'' && delimeter == -1)) {

			if (str[i] == '\'') {
				delimeter = i;
			}

			i++;
		}

		if (i == strlen(str) && delimeter >= 0) {
			str_correct = 1;
		} else {
			puts("��������� ������ ����������. ���������� ��� ���.\n");
		}

	} while (str_correct != 1);

	
	i = 0;
	while (i < delimeter) {
		ft *= 10;
		ft += str[i] - '0';
		i++;
	}

	while (str[++i] != 0) {
		inches *= 10;
		inches += str[i] - '0';
	}

	cm_height = (ft * 12 + inches)*CM_IN_INCH;
	printf("����: %.4f ��\n", cm_height);

	system("pause");

	return 0;
}