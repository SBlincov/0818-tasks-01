/*Программа подсчитывает сумму чисел во введеной строке*/
#include <stdio.h>
#define SIZE 255

int main()
{
	char str[SIZE];
	int i, num, sum;
	num = 0;
	sum = 0;

	for(i = 0; i < SIZE; i++)
	{
		str[i] = 'a';
	}

	printf("Enter numbers (The maximum string length = 255 characters)\n");
	fgets(str, SIZE, stdin);
	
	for (i = 0; i < SIZE; i++)
	{
		if((str[i] >= '0') && (str[i] <= '9'))
		{
				num = (num * 10) + (str[i] - 48);
		} 
		else
		{
				sum += num;
				num = 0;
		}	
	}	
	
	printf("Sum of numbers: %d\n", sum);
	return 0;
}
