/*Преобразует строку цифр в символьном представлении, где группы разрядов отделены пробелами */
#include <stdio.h>
#define SIZE 255

int main()
{
        char str[SIZE];	
        int i, count;        
        count = 0;

        for(i = 0; i < SIZE; i++)
        {
                str[i] = 'a';		
        }

        printf("Enter numbers (The maximum string length = 255 characters)\n");
        fgets(str, SIZE, stdin);

        for(i = 0; i < SIZE; i++)
        {
                if((str[i] >= '0') && (str[i] <= '9'))
                {
                        str[count] = str[i];
			count ++;
                }                
        }		

	for(i = 0; i < count; i++)
	{		
		if(((count - i) % 3 == 0) && (i != 0))
		{
			printf(" %c", str[i]);			
		}
		else 
		{
			printf("%c", str[i]);			
		}
	}

	printf("\n");	
	return 0;
}
