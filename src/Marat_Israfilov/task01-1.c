/*Программа выводит сумму 15.20 в виде 15 руб. 20 коп. */
#include <stdio.h>
#include <locale.h>

int main()
{
	setlocale(LC_CTYPE, "rus");
	float num; 
	int tmp;
	printf("Введите сумму(руб.коп): \n");
	scanf("%f", &num);
	tmp = (int)num;
	printf("%d руб. %.0f коп.\n", tmp, (num-tmp)*100);
	return 0;
}
