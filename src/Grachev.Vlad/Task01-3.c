#include <stdio.h>
#include <string.h>
#define N 20
int main()
{
	char buf[N];
	int i,sum;
	puts("Enter a string");
	fgets(buf, N, stdin);
	buf[strlen(buf)-1]=0;
	sum = 0; i = 0;
	while (buf[i]){
		if ((buf[i] >= '0') && (buf[i] <= '9'))
			sum = sum + (buf[i] - '0');
		i++;
	}
	printf("Sum = %d \n", sum);
	return 0;
}