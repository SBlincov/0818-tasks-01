#include <stdio.h>
#include <string.h>
#define N 16
int main()
{
	char Num[N];
	int i,j;
	fgets(Num,N,stdin);
	Num[strlen(Num)-1] = 0;
	j = (strlen(Num) % 3) > 0 ? (3 - (strlen(Num) % 3)) : 0;
	for(i = 0; i < strlen(Num); i++)
		if( j < 3){
			printf("%c", Num[i]);
			j++;
		}
			else{
				printf(" %c", Num[i]);
				j = 1;
			}
	printf("\n");
	return 0;
}