// �������� ���������, ������� ����������� ����� � ���� "15.20" � ������� � � ���������� ����: "15 ���. 20 ���."
#include <stdio.h>
#include <locale.h>
#include <string.h>
#define STR_LENGTH 100

int main() {

	int i;
	char str[STR_LENGTH];

	setlocale(LC_ALL, "rus");

	puts("������� �����:");
	fgets(str, STR_LENGTH, stdin);
	str[strlen(str)-1] = 0; // \n -> \0

	// ������� �����
	i = 0;
	while (str[i] != '.' && str[i] != 0) {
		putchar(str[i++]);
	}

	printf(" ���. ");

	// ������� �������
	i++;
	if (str[i] >= '0' && str[i] <= '9') {
		putchar(str[i]);

		i++;
		if (str[i] >= '0' && str[i] <= '9') {
			putchar(str[i]);
		}

		printf(" ���.");
	}

	puts("");

	system("pause");

	return 0;
}