// ������� ������, ���������� ���������� ������������� �����, �������� "12345". ������������� � ������, ��� ������ �������� �������� ���������, �� ���� "12 345"
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <locale.h>
#include <string.h>
#define STR_SIZE 20

int main() {

	int i;
	int spaces;
	int distance = 0;
	char str[STR_SIZE];

	setlocale(LC_ALL, "rus");

	puts("������� ���������� ������������� �����:");
	fgets(str, STR_SIZE, stdin);
	str[strlen(str) - 1] = 0; // \n -> \0

	spaces = (strlen(str) - 1) / 3;

	for (i = 1; i <= spaces; i++) { // ��������� � ����� ������ ��������, ������� ����� �����������
		strcat(str, " ");
	}

	for (i = strlen(str)-1; i >= 0; i--) {

		if (distance == 3) {
			str[i] = ' ';
			spaces--;
			distance = 0;
		} else {
			str[i] = str[i - spaces];
			distance++;
		}
		
	}

	printf("%s\n", str);

	system("pause");

	return 0;
}