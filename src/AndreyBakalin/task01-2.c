#include <stdio.h>
#include <locale.h>
#define FTTOINCH 12
#define INCHTOCM 2.54

int main() {
	setlocale(LC_ALL, "rus");

	int i, ft = 0, inch = 0, apostroph, str_ok = 0;
	double result=0;
	char str[20];
	
	do {
		i = 0; apostroph = 0;
		puts("������� �������� ����� � ���� \"����'�����\"");
		fgets(str, 20, stdin);
		str[strlen(str) - 1] = 0;

		while ((str[i] >= '0' && str[i] <= '9') || (str[i] == '\'')) {
			if ((str[i] == '\'') && (i > 0) && (i < strlen(str)-1))
				apostroph++;
			i++;
		}
		if ((apostroph == 1) && (i == strlen(str)))
			str_ok = 1;
		else
			puts("������ ������� �������!\n");
	}
	while (str_ok != 1);
	
	i = 0;
	
	while (str[i] != '\'') {
		ft *= 10;
		ft += str[i] - '0';
		i++;
	}
	i++;
	while (str[i] != 0) {
		inch *= 10;
		inch += str[i] - '0';
		i++;
	}
	
	result = (ft * FTTOINCH + inch)*INCHTOCM;
	printf("�����: %.2lf cm\n", result);

	return 0;
}