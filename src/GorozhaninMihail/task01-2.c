#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <math.h>
int main()
{
	float sm_growth;
	int i, inch = 0, ft = 0;
	char inch_s[10], ft_s[10];
	printf("Enter your growth in feet (In feet and inches, with separator \' otherwise the program will throw an error): \n");
	scanf("%[0-9] %s", ft_s, inch_s);
	if (inch_s[0] == '\'')
	{
		for (i = 0; i < strlen(inch_s) - 1; i++)
			inch_s[i] = inch_s[i + 1];
		inch_s[strlen(inch_s) - 1] = 0;
		for (i = 0; i < strlen(ft_s); i++)
		{
			ft = ft * 10 + (int)(ft_s[i]-'0');
		}
		for (i = 0; i < strlen(inch_s); i++)
		{
			inch = inch*10 + (int)(inch_s[i] - '0');
		}
		sm_growth = (ft * 12 + inch)*2.54;
		printf("Your growth %.0f Cm.\n", sm_growth);
	}
	else
	    printf("Error!!\n");
		return 0;
}